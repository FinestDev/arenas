package org.apollodevs.arenas.modes;

import org.apollodevs.arenas.managers.ArenaManager;
import org.apollodevs.arenas.queue.Queue;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class Arena_1v1 implements CommandExecutor, Listener {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String args[]) {

		if (!(sender instanceof Player)) {
			sender.sendMessage("Only players can use arena commands!");
			return true;
		}

		Player p = (Player) sender;

		if (cmd.getName().equalsIgnoreCase("setlobby-1v1")) {
			ArenaManager.setLobby(p, "1v1");
			return true;
		}

		return true;
	}

	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		Player p = e.getEntity().getPlayer();
		Player killer = e.getEntity().getKiller();
		

		if (ArenaManager.one.contains(killer)) {
			ArenaManager.oneRemove(p, killer, "1v1");
			Queue.getNextMatch();
		}

	}

}
