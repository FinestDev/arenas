package org.apollodevs.arenas.modes;

import org.apollodevs.arenas.Main;
import org.apollodevs.arenas.libraries.ParticleEffect;
import org.apollodevs.arenas.managers.ArenaManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;

public class Arena_SnowFight implements Listener, CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (!(sender instanceof Player)) {
			sender.sendMessage("Only players can use arena commands!");
			return true;
		}

		Player p = (Player) sender;

		if (cmd.getName().equalsIgnoreCase("setlobby-sf")) {
			ArenaManager.setLobby(p, "SnowFight");
			return true;
		}
		return false;
	}


	/*
	 * Game Below
	 */

	@EventHandler
	public void onProjecile(ProjectileLaunchEvent e) {

		final Projectile p = e.getEntity();

		if (!(p instanceof Snowball)){
			return;
		}
		final Snowball s = (Snowball) p;

		final Player pl = (Player) p.getShooter();

		Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(), new Runnable() {
			public void run() {

				if (s.isDead() == true) {
					return;
				} else {
					if (ArenaManager.inGame(pl)) {
						ParticleEffect.FLAME.display(-1, -1, 0, 0, 0, s.getLocation().add(0, -0.2, 0), 500);
					} else {
						return;
					}
				}
			}
		}, 0L, 1L);
	}

	@EventHandler
	public void onProjectileHit(EntityDamageByEntityEvent e) {
		Projectile p = (Projectile) e.getDamager();
		Player pl = (Player) p.getShooter();

		Player t = (Player) e.getEntity();
		if (ArenaManager.inGame(pl)) {
			if (p instanceof Snowball) {
				if (!(pl == t)) {
					t.damage(3);
					if (t.getHealth() == 0) {
						ArenaManager.removeKilledPlayer(t, pl, "SnowFight");
					}
				}
			}
		}
	}
}
