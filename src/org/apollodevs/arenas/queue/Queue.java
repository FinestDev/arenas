package org.apollodevs.arenas.queue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apollodevs.arenas.managers.ArenaManager;
import org.apollodevs.arenas.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Queue {
	
	public static List<Player> queue = new ArrayList<>();
	
	public static LinkedHashMap<Player, Player> waiting = new LinkedHashMap<>();
	
	public static boolean getNextMatch(){
		Iterator<Map.Entry<Player, Player>> it = waiting.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<Player, Player> entry = it.next();
			Player p1 = entry.getKey();
			Player p2 = entry.getValue();
			match1v1(p1);
			match1v1(p2);
			it.remove();
			return true;
		}
		return false;
	}
	
	public static boolean match1v1(Player player){
		if(queue.contains(player)) return false;
		if(waiting.containsKey(player)) return false;
		queue.add(player);
		if(queue.size() == 0) return false;
		if(ArenaManager.one.size() >= 2 || queue.size() == 1){
			player.sendMessage(Utils.chat("&8[&3*&8] &7You have been added to the &e1v1 &7queue!"));
		}
		for(Player opponent : queue){
			if(!player.equals(opponent)){
				if(ArenaManager.one.size() >= 2){
					waiting.put(player, opponent);
				}else{
					queue.remove(player);
					queue.remove(opponent);
					ArenaManager.joinLobby(player, "1v1");
					ArenaManager.joinLobby(opponent, "1v1");
					ArenaManager.join1v1(player);
					ArenaManager.join1v1(opponent);
				}
			}
		}
		return false;
	}

}
