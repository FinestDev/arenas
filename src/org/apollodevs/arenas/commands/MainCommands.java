package org.apollodevs.arenas.commands;

import org.apollodevs.arenas.managers.ArenaManager;
import org.apollodevs.arenas.ui.ArenaSelectorUI;
import org.apollodevs.arenas.ui.ProfileUI;
import org.apollodevs.arenas.utils.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MainCommands implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		Player p = (Player) sender;

		if (cmd.getName().equalsIgnoreCase("set-spawn")) {
			ArenaManager.setSpawn(p);
			return true;
		}

		if (cmd.getName().equalsIgnoreCase("leave")) {

			if (ArenaManager.inGame(p)) {
				ArenaManager.removePlayer(p);
			} else {
				p.sendMessage(Utils.chat("&8[&3*&8] &7You are currently not in an arena!"));
				return true;
			}
			return true;
		}
		return false;
	}

}
