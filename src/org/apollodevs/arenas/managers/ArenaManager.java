package org.apollodevs.arenas.managers;

import java.util.ArrayList;

import org.apollodevs.arenas.Main;
import org.apollodevs.arenas.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class ArenaManager {

	/*
	 * ArrayList for players in games
	 */

	public static ArrayList<Player> snowfight = new ArrayList<Player>();

	public static ArrayList<Player> potionfactory = new ArrayList<Player>();
	
	public static ArrayList<Player> one = new ArrayList<Player>();

	/*
	 * Set Return Point
	 */

	public static void toSpawn(Player p) {
		Location spawn = new Location(
				Main.getPlugin().getServer().getWorld(Main.getPlugin().getConfig().getString("Arenas.Spawn.World")),
				Main.getPlugin().getConfig().getDouble("Arenas.Spawn.X"),
				Main.getPlugin().getConfig().getDouble("Arenas.Spawn.Y"),
				Main.getPlugin().getConfig().getDouble("Arenas.Spawn.Z"),
				Main.getPlugin().getConfig().getInt("Arenas.Spawn.Yaw"),
				Main.getPlugin().getConfig().getInt("Arenas.Spawn.Pitch"));
		p.teleport(spawn);
		p.getInventory().clear();
		addItems(p);
	}

	public static void setSpawn(Player p) {
		Location l = p.getLocation();
		Main.getPlugin().getConfig().set("Arenas.Spawn.X", Double.valueOf(l.getBlockX() + 0.5));
		Main.getPlugin().getConfig().set("Arenas.Spawn.Y", Double.valueOf(l.getBlockY() + 0.5));
		Main.getPlugin().getConfig().set("Arenas.Spawn.Z", Double.valueOf(l.getBlockZ() + 0.5));
		Main.getPlugin().getConfig().set("Arenas.Spawn.Yaw", Float.valueOf(l.getYaw()));
		Main.getPlugin().getConfig().set("Arenas.Spawn.Pitch", Float.valueOf(l.getPitch()));
		Main.getPlugin().getConfig().set("Arenas.Spawn.World", String.valueOf(l.getWorld().getName()));
		p.sendMessage(Utils.chat("&6&7Arenas &8> &7You have set the spawn point for the SPAWN"));
		Main.getPlugin().saveConfig();
	}

	/*
	 * Lobby Methods
	 */

	public static void joinLobby(Player p, String s) {
		Location arenalobby = new Location(
				Main.getPlugin().getServer()
						.getWorld(Main.getPlugin().getConfig().getString("Arenas.Lobby." + s + ".World")),
				Main.getPlugin().getConfig().getDouble("Arenas.Lobby." + s + ".X"),
				Main.getPlugin().getConfig().getDouble("Arenas.Lobby." + s + ".Y"),
				Main.getPlugin().getConfig().getDouble("Arenas.Lobby." + s + ".Z"),
				Main.getPlugin().getConfig().getInt("Arenas.Lobby." + s + ".Yaw"),
				Main.getPlugin().getConfig().getInt("Arenas.Lobby." + s + ".Pitch"));
		p.teleport(arenalobby);
		p.setGameMode(GameMode.SURVIVAL);
		p.getInventory().clear();
	}

	public static void setLobby(Player p, String s) {
		Location l = p.getLocation();
		Main.getPlugin().getConfig().set("Arenas.Lobby." + s + ".X", Double.valueOf(l.getBlockX() + 0.5));
		Main.getPlugin().getConfig().set("Arenas.Lobby." + s + ".Y", Double.valueOf(l.getBlockY() + 0.5));
		Main.getPlugin().getConfig().set("Arenas.Lobby." + s + ".Z", Double.valueOf(l.getBlockZ() + 0.5));
		Main.getPlugin().getConfig().set("Arenas.Lobby." + s + ".Yaw", Float.valueOf(l.getYaw()));
		Main.getPlugin().getConfig().set("Arenas.Lobby." + s + ".Pitch", Float.valueOf(l.getPitch()));
		Main.getPlugin().getConfig().set("Arenas.Lobby." + s + ".World", String.valueOf(l.getWorld().getName()));
		p.sendMessage(Utils.chat("&bArenas &8> &7You have set the lobby for the arena &f&l" + s));
		Main.getPlugin().saveConfig();
	}

	/*
	 * In Game Methods
	 */

	public static void joinSF(Player p) {
		snowfight.add(p);
		p.sendMessage(Utils.chat("&f&lSnowFight &8> &7You have joined the arena. FIGHT!"));

		for (Player pl : Bukkit.getOnlinePlayers()) {
			if (snowfight.contains(pl)) {
				pl.sendMessage(Utils.chat("&8[&3*&8] " + p.getDisplayName() + " &7has &bjoined &7the game!"));
				ItemStack snowball;

				ArrayList<String> lore = new ArrayList<String>();

				Inventory inv = p.getInventory();
				snowball = new ItemStack(Material.SNOW_BALL, 64);
				ItemMeta meta;

				meta = snowball.getItemMeta();
				meta.setDisplayName(Utils.chat("&f&lSnowFight &fSnowball"));
				lore.clear();
				lore.add("");
				lore.add(Utils.chat("&7With these special snowballs you"));
				lore.add(Utils.chat("&7can damage other players by simply"));
				lore.add(Utils.chat("&7throwing at your opponent!"));
				lore.add(Utils.chat("&7"));
				lore.add(Utils.chat("&6TIP: Unlock particle trails in"));
				lore.add(Utils.chat("&6the shop"));
				lore.add(Utils.chat("&7"));
				lore.add(Utils.chat("&e&lParticle Trail > &fDefault"));
				meta.setLore(lore);
				snowball.setItemMeta(meta);
				inv.setItem(0, snowball);
				inv.setItem(1, snowball);
				inv.setItem(2, snowball);
				inv.setItem(3, snowball);
				inv.setItem(4, snowball);
				inv.setItem(5, snowball);
			}
		}
	}

	public static void joinPF(Player p) {
		potionfactory.add(p);
		p.sendMessage(Utils.chat("&5&lPotionFactory &8> &7You have joined the arena. FIGHT!"));

		for (Player pl : Bukkit.getOnlinePlayers()) {
			if (snowfight.contains(pl)) {
				pl.sendMessage(Utils.chat("&8[&3*&8] " + p.getDisplayName() + " &7has &5joined &7the game!"));
			}
		}
		
		ItemStack sword, hel, ches, leg, boot;

		ArrayList<String> lore = new ArrayList<String>();

		Inventory inv = p.getInventory();
		ItemMeta meta;
		
		sword = new ItemStack(Material.DIAMOND_SWORD, 1);
		
		sword.addEnchantment(Enchantment.DAMAGE_ALL, 2);
		meta = sword.getItemMeta();
		meta.setDisplayName(Utils.chat("&5Chemist Shank"));
		lore.clear();
		lore.add("");
		lore.add(Utils.chat("&7With these special snowballs you"));
		lore.add(Utils.chat("&7can damage other players by simply"));
		lore.add(Utils.chat("&7throwing at your opponent!"));
		lore.add(Utils.chat("&7"));
		lore.add(Utils.chat("&6TIP: Unlock custom enchants"));
		lore.add(Utils.chat("&6in the shop"));
		meta.setLore(lore);
		sword.setItemMeta(meta);
		
		inv.setItem(0, sword);
	
	}
	
	public static void join1v1(Player p) {
		one.add(p);
		p.sendMessage(Utils.chat("&e&l1v1 &8> &7You have joined the arena. FIGHT!"));

		for (Player pl : Bukkit.getOnlinePlayers()) {
			if (snowfight.contains(pl)) {
				pl.sendMessage(Utils.chat("&8[&3*&8] " + p.getDisplayName() + " &7has &5joined &7the game!"));
			}
		}
		
		ItemStack sword, hel, ches, leg, boot;

		ArrayList<String> lore = new ArrayList<String>();

		Inventory inv = p.getInventory();
		ItemMeta meta;
		
		sword = new ItemStack(Material.DIAMOND_SWORD, 1);
		
		sword.addEnchantment(Enchantment.DAMAGE_ALL, 2);
		meta = sword.getItemMeta();
		meta.setDisplayName(Utils.chat("&5Chemist Shank"));
		lore.clear();
		lore.add("");
		lore.add(Utils.chat("&7With these special snowballs you"));
		lore.add(Utils.chat("&7can damage other players by simply"));
		lore.add(Utils.chat("&7throwing at your opponent!"));
		lore.add(Utils.chat("&7"));
		lore.add(Utils.chat("&6TIP: Unlock custom enchants"));
		lore.add(Utils.chat("&6in the shop"));
		meta.setLore(lore);
		sword.setItemMeta(meta);
		
		inv.setItem(0, sword);
	
	}

	public static boolean inGame(Player p) {
		if (snowfight.contains(p)) {
			return true;
		} else if (potionfactory.contains(p)) {
			return true;
		}else if (one.contains(p)) {
			return true;
		}
		return false;
	}

	public static void removeKilledPlayer(Player p, Player target, String s) {
		snowfight.remove(p);
		potionfactory.remove(p);
		one.remove(p);
		PlayerManager.addDeaths(p, s, 1);
		PlayerManager.addKills(target, s, 1);
		PlayerManager.addTokens(target, 5);
		p.sendMessage(Utils.chat("&8[&3*&8] &7You have been killed by " + target.getDisplayName()));
		target.sendMessage(
				Utils.chat("&8[&3*&8] &7You have killed " + p.getDisplayName() + " &7and gained &a+5 tokens"));
	}
	
	public static void oneRemove(Player p, Player target, String s) {
		snowfight.remove(p);
		potionfactory.remove(p);
		snowfight.remove(target);
		potionfactory.remove(target);
		one.remove(p);
		one.remove(target);
		PlayerManager.addDeaths(p, s, 1);
		PlayerManager.addKills(target, s, 1);
		PlayerManager.addTokens(target, 5);
		toSpawn(target);
		p.sendMessage(Utils.chat("&8[&3*&8] &7You have been killed by " + target.getDisplayName()));
		target.sendMessage(
				Utils.chat("&8[&3*&8] &7You have killed " + p.getDisplayName() + " &7and gained &a+5 tokens"));
	}

	public static void removePlayer(Player p) {
		snowfight.remove(p);
		potionfactory.remove(p);
		one.remove(p);
		toSpawn(p);
		p.sendMessage(Utils.chat("&f&l&3&lArenas &8> &cYou have left the arena!"));

		for (Player pl : Bukkit.getOnlinePlayers()) {
			if (snowfight.contains(pl)) {
				pl.sendMessage(Utils.chat("&8[&3*&8] " + p.getDisplayName() + " &7has &cleft &7the arena!"));
			}
		}
		p.getInventory().clear();
		addItems(p);
	}

	public static void addItems(Player p) {

		ItemStack mode_selector, cosmetics;

		ArrayList<String> lore = new ArrayList<String>();

		Inventory inv = p.getInventory();
		SkullMeta skull = (SkullMeta) Bukkit.getItemFactory().getItemMeta(Material.SKULL_ITEM);
		ItemStack stack = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		skull.setOwner(p.getName());
		skull.setDisplayName(Utils.chat("&bProfile &8(&6Click to View&8)"));
		lore.clear();
		lore.add("");
		lore.add(Utils.chat("&7Click on this item to open"));
		lore.add(Utils.chat("&7your &bprofile&7. Where you will"));
		lore.add(Utils.chat("&7be able to view your stats &"));
		lore.add(Utils.chat("&7settings!"));
		skull.setLore(lore);
		stack.setItemMeta(skull);
		inv.clear();
		inv.setItem(8, stack);

		ItemMeta meta;

		mode_selector = new ItemStack(Material.COMPASS, 1);

		meta = mode_selector.getItemMeta();
		meta.setDisplayName(Utils.chat("&bArena &7Selector &8(&6Click to View&8)"));
		lore.clear();
		lore.add("");
		lore.add(Utils.chat("&7View our new custom mode &barenas,"));
		lore.add(Utils.chat("&7fully custom made by our own staff"));
		lore.add(Utils.chat("&7team! &6Join now &7to have the experience"));
		lore.add(Utils.chat("&7of a lifetime!"));
		meta.setLore(lore);
		mode_selector.setItemMeta(meta);
		inv.setItem(0, mode_selector);

		cosmetics = new ItemStack(Material.ENDER_CHEST, 1);

		meta = cosmetics.getItemMeta();
		meta.setDisplayName(Utils.chat("&bCosmetics &8(&6Click to View&8)"));
		lore.clear();
		lore.add("");
		lore.add(Utils.chat("&7View our &bfully &7custom cosmetics"));
		lore.add(Utils.chat("&7system. It contains some very"));
		lore.add(Utils.chat("&7amazing &bfeatures &7that you should"));
		lore.add(Utils.chat("&7check out!"));
		meta.setLore(lore);
		cosmetics.setItemMeta(meta);
		inv.setItem(7, cosmetics);
	}
}
