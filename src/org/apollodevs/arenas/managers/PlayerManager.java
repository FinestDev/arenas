package org.apollodevs.arenas.managers;

import org.apollodevs.arenas.Main;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public class PlayerManager {

	public static void addTokens(Player p, int i) {
		int tokens = Main.getPlugin().getConfig().getInt("Players." + p.getName() + ".Tokens");
		Main.getPlugin().getConfig().set("Players." + p.getName() + ".Tokens", tokens + i);
		Main.getPlugin().saveConfig();
	}

	public static void removeTokens(Player p, int i) {
		int tokens = Main.getPlugin().getConfig().getInt("Players." + p.getName() + ".Tokens");
		Main.getPlugin().getConfig().set("Players." + p.getName() + ".Tokens", tokens - i);
		Main.getPlugin().saveConfig();
	}

	public static int getTokens(Player p) {
		if(!hasInfo(p)){
			return 0;
		}
		return Main.getPlugin().getConfig().getInt("Players." + p.getName() + ".Tokens");
	}

	/*
	 * Kills, Death, Kill Death Ratio Manager
	 */

	public static void addKills(Player p, String s, int i) {
		int kills = Main.getPlugin().getConfig().getInt("Players." + p.getName() + "." + s + ".Kills");
		Main.getPlugin().getConfig().set("Players." + p.getName() + "." + s + ".Kills", kills + i);
		Main.getPlugin().saveConfig();
	}

	public static void addDeaths(Player p, String s, int i) {
		int deaths = Main.getPlugin().getConfig().getInt("Players." + p.getName() + "." + s + ".Deaths");
		Main.getPlugin().getConfig().set("Players." + p.getName() + "." + s + ".Deaths", deaths + i);
		Main.getPlugin().saveConfig();
	}
	
	
	public static void addKills(OfflinePlayer p, String s, int i) {
		int kills = Main.getPlugin().getConfig().getInt("Players." + p.getName() + "." + s + ".Kills");
		Main.getPlugin().getConfig().set("Players." + p.getName() + "." + s + ".Kills", kills + i);
		Main.getPlugin().saveConfig();
	}

	public static void addDeaths(OfflinePlayer p, String s, int i) {
		int deaths = Main.getPlugin().getConfig().getInt("Players." + p.getName() + "." + s + ".Deaths");
		Main.getPlugin().getConfig().set("Players." + p.getName() + "." + s + ".Deaths", deaths + i);
		Main.getPlugin().saveConfig();
	}
	

	public static int getKills(Player p, String s) {
		if(!hasInfo(p)){
			return 0;
		}
		return Main.getPlugin().getConfig().getInt("Players." + p.getName() + "." + s + ".Kills");
	}

	public static int getDeaths(Player p, String s) {
		if(!hasInfo(p)){
			return 0;
		}
		return Main.getPlugin().getConfig().getInt("Players." + p.getName() + "." + s + ".Deaths");
	}
	
	
	
	public static int getKills(OfflinePlayer p, String s) {
		if(!hasInfo(p)){
			return 0;
		}
		return Main.getPlugin().getConfig().getInt("Players." + p.getName() + "." + s + ".Kills");
	}

	public static int getDeaths(OfflinePlayer p, String s) {
		if(!hasInfo(p)){
			return 0;
		}
		return Main.getPlugin().getConfig().getInt("Players." + p.getName() + "." + s + ".Deaths");
	}
	
	
	
	
	public static boolean hasInfo(Player p){
		if(Main.getPlugin().getConfig().contains("Players." + p.getName())){
			return true;
		}else{
			return false;
		}
		
	}
	
	public static boolean hasInfo(OfflinePlayer p){
		if(Main.getPlugin().getConfig().contains("Players." + p.getName())){
			return true;
		}else{
			return false;
		}
		
	}
}
