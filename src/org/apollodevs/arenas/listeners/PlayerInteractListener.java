package org.apollodevs.arenas.listeners;

import org.apollodevs.arenas.ui.ArenaSelectorUI;
import org.apollodevs.arenas.ui.ProfileUI;
import org.apollodevs.arenas.utils.Utils;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerInteractListener implements Listener {

	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		Player p = e.getPlayer();

		if (e.getAction() == Action.PHYSICAL) {
			e.setCancelled(true);
			return;
		}

		if (p.getItemInHand() == null || p.getItemInHand().getType().equals(Material.AIR)
				|| !p.getItemInHand().hasItemMeta()) {
			return;
		}
		if (p.getItemInHand().getItemMeta().getDisplayName()
				.equalsIgnoreCase(Utils.chat("&bArena &7Selector &8(&6Click to View&8)"))) {
			p.openInventory(ArenaSelectorUI.GUI(p));
		} else if (p.getItemInHand().getItemMeta().getDisplayName()
				.equalsIgnoreCase(Utils.chat("&bProfile &8(&6Click to View&8)"))) {
			p.openInventory(ProfileUI.GUI(p));
		}
	}
}
