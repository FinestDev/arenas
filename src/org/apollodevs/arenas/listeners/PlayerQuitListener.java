package org.apollodevs.arenas.listeners;

import org.apollodevs.arenas.managers.ArenaManager;
import org.apollodevs.arenas.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener {
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		Player p = e.getPlayer();
		
		if (ArenaManager.snowfight.contains(p)){
			for (Player pl : Bukkit.getOnlinePlayers()){
				if (ArenaManager.snowfight.contains(pl)){
					pl.sendMessage(Utils.chat("&8[&3*&8] " + p.getDisplayName() + " &7has &cleft &7the arena!"));
				}
			}
		}
		ArenaManager.removePlayer(p);
	}

}
