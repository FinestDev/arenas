package org.apollodevs.arenas.listeners;

import org.apollodevs.arenas.ui.ArenaSelectorUI;
import org.apollodevs.arenas.ui.ProfileUI;
import org.apollodevs.arenas.utils.Utils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryClickListener implements Listener {

	@EventHandler
	public void onInvClick(InventoryClickEvent e) {
		String title = e.getInventory().getTitle();

		if (e.getCurrentItem() == null || e.getCurrentItem().getItemMeta() == null) {
			return;
		}

		Player p = (Player) e.getWhoClicked();

		if (e.getCurrentItem().getItemMeta().getDisplayName()
				.equalsIgnoreCase(Utils.chat("&b" + p.getName() + "(s) &7Profile &8(&6Click to View&8)"))) {
			e.setCancelled(true);
		} else if (e.getCurrentItem().getItemMeta().getDisplayName()
				.equalsIgnoreCase(Utils.chat("&bArena &7Selector &8(&6Click to View&8)"))) {
			e.setCancelled(true);
		}

		if (title.equals(ArenaSelectorUI.inventory_name)) {
			e.setCancelled(true);

			ArenaSelectorUI.clicked((Player) e.getWhoClicked(), e.getSlot(), e.getCurrentItem(), e.getInventory());
		} else if (title.equals(ProfileUI.inventory_name)) {
			e.setCancelled(true);

			ProfileUI.clicked((Player) e.getWhoClicked(), e.getSlot(), e.getCurrentItem(), e.getInventory());
		}
	}
}
