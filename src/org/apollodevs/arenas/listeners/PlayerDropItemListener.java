package org.apollodevs.arenas.listeners;

import org.apollodevs.arenas.utils.Utils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

public class PlayerDropItemListener implements Listener {

	@EventHandler
	public void onDrop(PlayerDropItemEvent e) {

		Player p = e.getPlayer();

		if (p.getItemInHand().getItemMeta().getDisplayName()
				.equalsIgnoreCase(Utils.chat("&bArena &7Selector &8(&6Click to View&8)"))) {
			e.setCancelled(true);
		}

		else if (p.getItemInHand().getItemMeta().getDisplayName()
				.equalsIgnoreCase(Utils.chat("&b" + p.getName() + "(s) &7Profile &8(&6Click to View&8)"))) {
			e.setCancelled(true);
		}
	}
}
