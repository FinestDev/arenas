package org.apollodevs.arenas.listeners;

import java.util.ArrayList;
import java.util.List;

import org.apollodevs.arenas.Main;
import org.apollodevs.arenas.managers.ArenaManager;
import org.apollodevs.arenas.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class PlayerJoinListener implements Listener {
	
	public static ItemStack mode_selector;
	
	List<String> lore = new ArrayList<String>();

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		
		final Player p = e.getPlayer();
		
		ArenaManager.toSpawn(p);
		
		if (!Main.getPlugin().getConfig().contains("Players." + p.getName())) {
			Main.getPlugin().getConfig().set("Players." + p.getName() + ".Tokens", 0);
			Main.getPlugin().getConfig().set("Players." + p.getName() + ".SnowFight" +  ".Kills", 0);
			Main.getPlugin().getConfig().set("Players." + p.getName() + ".SnowFight" + ".Deaths", 0);
			Main.getPlugin().getConfig().set("Players." + p.getName() + ".PotionFactory" +  ".Kills", 0);
			Main.getPlugin().getConfig().set("Players." + p.getName() + ".PotionFactory" + ".Deaths", 0);
			Main.getPlugin().getConfig().set("Players." + p.getName() + ".1v1" +  ".Kills", 0);
			Main.getPlugin().getConfig().set("Players." + p.getName() + ".1v1" + ".Deaths", 0);
			Main.getPlugin().saveConfig();
		}
		
		Player p1 = e.getPlayer();
		Inventory inv = e.getPlayer().getInventory();
		SkullMeta  skull = (SkullMeta) Bukkit.getItemFactory().getItemMeta(Material.SKULL_ITEM);
		ItemStack stack = new ItemStack(Material.SKULL_ITEM,1 , (byte)3);
		skull.setOwner(p1.getName());
		skull.setDisplayName(Utils.chat("&bProfile &8(&6Click to View&8)"));
		lore.clear();
		lore.add("");
		lore.add(Utils.chat("&7Click on this item to open"));
		lore.add(Utils.chat("&7your &bprofile&7. Where you will"));
		lore.add(Utils.chat("&7be able to view your stats &"));
		lore.add(Utils.chat("&7settings!"));
		skull.setLore(lore);
		stack.setItemMeta(skull);
		inv.clear();
		inv.setItem(8, stack);
		
		ItemMeta meta;

		mode_selector = new ItemStack(Material.COMPASS, 1);

		meta = mode_selector.getItemMeta();
		meta.setDisplayName(Utils.chat("&bArena &7Selector &8(&6Click to View&8)"));
		lore.clear();
		lore.add("");
		lore.add(Utils.chat("&7View our new custom mode &barenas,"));
		lore.add(Utils.chat("&7fully custom made by our own staff"));
		lore.add(Utils.chat("&7team! &6Join now &7to have the experience"));
		lore.add(Utils.chat("&7of a lifetime!"));
		meta.setLore(lore);
		mode_selector.setItemMeta(meta);
		inv.setItem(0, mode_selector);
	}
}
