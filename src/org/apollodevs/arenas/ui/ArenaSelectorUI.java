package org.apollodevs.arenas.ui;

import java.util.ArrayList;
import java.util.List;

import org.apollodevs.arenas.managers.ArenaManager;
import org.apollodevs.arenas.queue.Queue;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ArenaSelectorUI {

	public static Inventory inv;
	public static String inventory_name;
	public static ItemStack snowfight, potionfactory, one;

	public static ArrayList<String> lore = new ArrayList<String>();

	public static void initialize() {
		inventory_name = "�7Arenas";

		inv = Bukkit.createInventory(null, 27);

	}

	public static Inventory GUI(Player p) {

		Inventory toReturn = Bukkit.createInventory(null, 27, inventory_name);

		ItemMeta meta;
		List<String> lore = new ArrayList<String>();

		snowfight = new ItemStack(Material.SNOW_BALL, 1);

		meta = snowfight.getItemMeta();
		meta.setDisplayName("�fSnowFight �8(�6Click to Join�8)");
		lore.clear();
		lore.add("�7Use snowballs as your main weapon!");
		lore.add("�7Face against your friends in our epic");
		lore.add("�7winter wonderland map. �fJoin Now!");
		lore.add("");
		lore.add("�cPlayers In Game �8/ �b" + ArenaManager.snowfight.size());
		meta.setLore(lore);
		snowfight.setItemMeta(meta);
		
		potionfactory = new ItemStack(Material.POTION, 1);

		meta = potionfactory.getItemMeta();
		meta.setDisplayName("�5PotionFactory �8(�6Click to Join�8)");
		lore.clear();
		lore.add("�7Use potions as your main defence!");
		lore.add("�7Face against your friends in our crazy");
		lore.add("�7custom PotionFactory map. �fJoin Now!");
		lore.add("");
		lore.add("�cPlayers In Game �8/ �b" + ArenaManager.potionfactory.size());
		meta.setLore(lore);
		potionfactory.setItemMeta(meta);
		
		one = new ItemStack(Material.IRON_SWORD, 1);

		meta = one.getItemMeta();
		meta.setDisplayName("�e1v1 �8(�6Click to Join�8)");
		lore.clear();
		lore.add("�7Use potions as your main defence!");
		lore.add("�7Face against your friends in our crazy");
		lore.add("�7custom PotionFactory map. �fJoin Now!");
		lore.add("");
		lore.add("�cPlayers Ahead of You �8/ �b" + Queue.waiting.size());
		meta.setLore(lore);
		one.setItemMeta(meta);

		inv.setItem(0, snowfight);
		inv.setItem(1, potionfactory);
		inv.setItem(2, one);

		toReturn.setContents(inv.getContents());
		return toReturn;
	}

	public static void clicked(final Player p, int slot, ItemStack clicked, Inventory inv) {

		if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase("�fSnowFight �8(�6Click to Join�8)")) {
			ArenaManager.joinLobby(p, "SnowFight");
			ArenaManager.joinSF(p);
			if(Queue.queue.contains(p) || Queue.waiting.containsKey(p)){
				Queue.queue.remove(p);
				Queue.waiting.remove(p);
			}
		} else if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase("�5PotionFactory �8(�6Click to Join�8)")) {
			ArenaManager.joinLobby(p, "PotionFactory");
			ArenaManager.joinPF(p);
			if(Queue.queue.contains(p) || Queue.waiting.containsKey(p)){
				Queue.queue.remove(p);
				Queue.waiting.remove(p);
			}
		} else if (clicked.getItemMeta().getDisplayName().equalsIgnoreCase("�e1v1 �8(�6Click to Join�8)")) {
			Queue.match1v1(p);
		}
	}
}