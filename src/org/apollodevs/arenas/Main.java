package org.apollodevs.arenas;

import org.apollodevs.arenas.commands.MainCommands;
import org.apollodevs.arenas.listeners.InventoryClickListener;
import org.apollodevs.arenas.listeners.LeaveListener;
import org.apollodevs.arenas.listeners.PlayerDeathListener;
import org.apollodevs.arenas.listeners.PlayerDropItemListener;
import org.apollodevs.arenas.listeners.PlayerInteractListener;
import org.apollodevs.arenas.listeners.PlayerJoinListener;
import org.apollodevs.arenas.listeners.PlayerQuitListener;
import org.apollodevs.arenas.listeners.PlayerRespawnListener;
import org.apollodevs.arenas.managers.ArenaManager;
import org.apollodevs.arenas.modes.Arena_1v1;
import org.apollodevs.arenas.modes.Arena_PotionFactory;
import org.apollodevs.arenas.modes.Arena_SnowFight;
import org.apollodevs.arenas.ui.ArenaSelectorUI;
import org.apollodevs.arenas.ui.ProfileUI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	private static Main plugin;

	public static Main getPlugin() {
		return plugin;
	}

	public void onEnable() {
		plugin = this;
		

		saveDefaultConfig();

		ArenaSelectorUI.initialize();
		ProfileUI.initialize();

		getCommand("setlobby-sf").setExecutor(new Arena_SnowFight());
		getCommand("setlobby-pf").setExecutor(new Arena_PotionFactory());
		getCommand("set-spawn").setExecutor(new MainCommands());
		getCommand("leave").setExecutor(new MainCommands());
		getCommand("setlobby-1v1").setExecutor(new Arena_1v1());

		Bukkit.getPluginManager().registerEvents(new PlayerQuitListener(), this);
		Bukkit.getPluginManager().registerEvents(new LeaveListener(), this);
		Bukkit.getPluginManager().registerEvents(new Arena_SnowFight(), this);
		Bukkit.getPluginManager().registerEvents(new Arena_PotionFactory(), this);
		Bukkit.getPluginManager().registerEvents(new Arena_1v1(), this);
		Bukkit.getPluginManager().registerEvents(new InventoryClickListener(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerDeathListener(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerInteractListener(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerDropItemListener(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerRespawnListener(), this);

		for (Player p : Bukkit.getOnlinePlayers()) {
			p.getInventory().clear();
			ArenaManager.addItems(p);
		}
	}

	public void onDisable() {
		ArenaManager.snowfight.clear();
		ArenaManager.potionfactory.clear();
	}

}
